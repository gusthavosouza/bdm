# (Banco de Modelos)
[TOC]
##### URL BASE: www.bancodemodelos.com.br

### 0 - Acesso
>**Header Params** 
Enviar os headers com o código do facebook e com o token de acesso nas solicitaçõe de update de url, de acesso ou atualizações do WebView.
Nesse caso mesmo quando o servidor reiniciar ou a sessão do usuário expirar o login será feito automaticamente e não será redirecionado ao logout.

### 1 - Login

**GET** - /login/modelo/
>**Header Params**
```
     facebookId
     accessToken
```
Response Ok se já estiver cadastrado:
##### /modelo/

Response Ok para se cadastrar
##### /nome/

Response não autorizado:
##### /unauthorized/

### 2 - Logout

Após o logout na página será redirecionado para.

##### /logout/

### 3 - Error

Configurando...

### 4 - Notificação

Atualização/Cadastro do token de notificação gerado pelo FCM(FireBase Cloud Messaging)

**POST** - /rest/notification/token/
>Content-Type: application/json
```
{
     "facebookId":"1234",
     "fcmRegistrationToken":"ABC123aBc",
     "tipoDispositivo":1,
     "facebookToken":"ABC123aBc" // em fase de testes

}
```
>Response SUCCESS

* **200** - Ok Id do dispositivo salvo com sucesso

>Response FAILURE

* **401** - token do facebook invalido e/ou usuario não autorizado na plataforma
* **500** - erro no servidor

### 4.1 - TipoDispositivo

Os tipos de dispositivos podem ser configurados de acordo com a plataforma

* **1** - Android
* **2** - iOS
* **3** - Web (futuro)

### 5 - Fotos

### 5.1 Fotos modelos
Para recuperar uma foto de um determinado usuario

** GET ** - /rest/foto/{facebookId}/{nomeFoto}

Para buscar a foto de perfil nomeFoto  deve ser perfil_.jpg

* nomeFoto de 1~6 .jpg

Response 
"image/png" ou "image/jpg"

### 5.2 Fotos/Banner Vaga

** GET ** - /rest/foto/vaga/{Id da Empresa}/{Id da Vaga}

Response 
"image/png" ou "image/jpg"